ARG DOCKER_REGISTRY=docker.io
ARG DOCKER_NAMESPACE=agrozyme
FROM ${DOCKER_REGISTRY}/${DOCKER_NAMESPACE}/php
COPY rootfs /
ENV DRUSH_LAUNCHER_FALLBACK=/usr/local/bin/drush-ops
RUN chmod +x /usr/local/bin/* \
  && gem update -N docker_core \
  && gem clean \
  && /usr/local/bin/docker_build.rb
WORKDIR /var/www/html
CMD ["/usr/local/bin/docker_run.rb"]
