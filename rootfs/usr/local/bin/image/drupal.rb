# require_relative('../docker_core')
require('docker_core')

module DockerCore
  module Image
    module Drupal
      module Build
        # @param [String] path
        def self.download_drupal_console(path)
          repository = 'hechoendrupal/drupal-console-launcher'
          version = Shell.github_latest_version(repository)
          uri = "https://github.com/#{repository}/releases/download/#{version}/drupal.phar"
          file = "#{path}/drupal"
          Shell.download_file(uri, file)
        end

        # @param [String] path
        def self.download_drush(path)
          repository = 'drush-ops/drush-launcher'
          version = Shell.github_latest_version(repository)
          uri = "https://github.com/#{repository}/releases/download/#{version}/drush.phar"
          file = "#{path}/drush"
          Shell.download_file(uri, file)

          # Fallback only 8.x has phar file
          version = '8.4.12'
          uri = "https://github.com/drush-ops/drush/releases/download/#{version}/drush.phar"
          file = "#{path}/drush-ops"
          Shell.download_file(uri, file)
        end

        # @param [String] file
        def self.update_composer_json(file)
          text = File.read(file)
          json = JSON.parse(text)
          json['repositories'] << { type: 'composer', url: 'https://asset-packagist.org' }
          json['extra']['patchLevel'] = { 'drupal/core' => '-p2' }
          json['extra']['enable-patching'] = true
          json['extra']['composer-exit-on-patch-failure'] = true
          json['extra']['installer-types'] = %w[ckeditor-plugin npm-asset bower-asset]
          json['extra']['installer-paths']['web/libraries/ckeditor/plugins/{$name}'] = ['type:ckeditor-plugin']
          json['extra']['installer-paths']['web/libraries/{$name}'] << 'type:bower-asset'
          json['extra']['installer-paths']['web/libraries/{$name}'] << 'type:npm-asset'
          json['config']['allow-plugins']['cweagans/composer-patches'] = true
          json['config']['allow-plugins']['oomphinc/composer-installers-extender'] = true
          text = JSON.pretty_generate(json)
          File.write(file, text)
        end

        def self.drupal_setup
          # version = '9.5.11'
          version = '11.0.9'
          www = '/var/www'
          project = '/tmp/drupal-project'

          System.run("composer create-project --no-install --no-interaction --no-progress drupal/recommended-project:#{version} #{project}")
          self.update_composer_json("#{project}/composer.json")
          System.run("composer require --no-interaction --no-update --no-progress --sort-packages --working-dir=#{project} cweagans/composer-patches oomphinc/composer-installers-extender")
          System.run("composer require --no-interaction --no-update --no-progress --sort-packages --working-dir=#{project} drush/drush:^13")
          Shell.remove_paths("#{project}/composer.lock")
          System.run("composer install --no-interaction --no-progress --working-dir=#{project}")
          # System.run("composer suggests --all --working-dir=#{project}")
          # System.run('drush', { root: project }, 'list')
          # System.run('drush', { root: project }, 'core:status')
          Shell.deflate_file("#{www}/drupal-project.tgz", project)
          Shell.remove_paths(project)
        end

        def self.main
          path = '/usr/local/bin'
          System.run('apk add --no-cache mariadb-client postgresql-bdr-client postgresql-client openssh-client-default rsync')
          # System.invoke('Download Drush', self.method(:download_drush), path)
          # System.invoke('Download Drupal Console', self.method(:download_drupal_console), path)
          Shell.change_mode('+x', "#{path}/*")
          System.invoke('Drupal Setup', self.method(:drupal_setup))
          System.run('composer clear-cache')
        end
      end

      module Run
        # @param [String] text
        def self.get_setting_pattern(text)
          return %r`^[#\s]*(\$settings\[[\s]*['"]#{text}['"][\s]*\])[\s]*=[\s]*(.*)$`i
        end

        # @param [String] text
        # @param [Array<String>] items
        def self.remove_settings(text, *items)
          items.each do |item|
            pattern = self.get_setting_pattern(item)
            text = text.gsub(pattern, '')
          end

          return text
        end

        # @param [String] text
        def self.update_class_loader(text)
          switch = Paser.boolean(ENV['DRUPAL_CLASS_LOADER_AUTO_DETECT'])
          pattern = self.get_setting_pattern('class_loader_auto_detect')
          replace = switch ? '# \1 = \2;' : '\1 = FALSE;'
          return text.gsub(pattern, replace)
        end

        # @param [String] text
        def self.update_config_private(text)
          pattern = self.get_setting_pattern('file_private_path')
          replace = '\1 = "sites/default/private";'
          return text.gsub(pattern, replace)
        end

        # @param [String] text
        def self.update_reverse_proxy(text)
          switch = ENV.fetch('DRUPAL_REVERSE_PROXY', '')
          pattern = self.get_setting_pattern('reverse_proxy')

          case "#{switch}".downcase
          when 'none'
            return text.gsub(pattern, '# \1 = \2')
          when 'traefik'
            return text.gsub(pattern, '# \1 = TRUE')
          else return text
          end
        end

        # @param [Array<String>] files
        def self.update_all_settings(*files)
          files.each do |file|
            if false == File.file?(file)
              next
            end

            text = File.read(file)
            text = self.update_class_loader(text)
            text = self.update_config_private(text)
            text = self.update_reverse_proxy(text)
            text = self.remove_settings(text, 'default_content_deploy_content_directory', 'config_sync_directory')
            items = [text, "$settings['default_content_deploy_content_directory'] = '../content';", "$settings['config_sync_directory'] = '../config/sync';", '']
            File.write(file, items.join("\n"))
          end
        end

        # @param [String] html
        def self.create_project(html)
          if false == File.file?("#{html}/composer.json")
            Shell.inflate_file("#{html}/../drupal-project.tgz", html)
          end

          Shell.change_owner(html)
        end

        def self.update_security(html)
          if false == Paser.boolean(ENV['DRUPAL_SECURITY'])
            return
          end

          Shell.remove_paths("#{html}/web/robots.txt")
        end

        def self.main
          html = '/var/www/html'
          default = "#{html}/web/sites/default"
          fpm = "php-fpm#{ENV['PHP_MAJOR_VERSION']}"

          Shell.update_user
          System.invoke('Create Project', self.method(:create_project), html)
          Shell.remove_paths("#{default}/files/config_*/")
          Shell.make_folders("#{default}/private", "#{html}/config/sync", "#{html}/content")
          System.invoke('Update All Settings', self.method(:update_all_settings), "#{default}/default.settings.php", "#{default}/settings.php")
          System.invoke('Update Security', self.method(:update_security), html)
          Shell.change_mode('u+w', default)

          Shell.change_owner(html)
          Shell.clear_folders("/run/#{fpm}")
          System.execute(fpm, { nodaemonize: true }, '')
        end
      end
    end
  end
end
